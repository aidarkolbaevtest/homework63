<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $password = password_hash('admin', PASSWORD_DEFAULT);
        $user
            ->setUsername('admin')
            ->setEmail('admin@admin.com')
            ->setPassword($password)
            ->setEnabled(1);
        $manager->persist($user);

        $manager->flush();
    }

}