<?php

namespace AppBundle\Features\Context;

use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @When /^я перехожу по ссылке для авторизации$/
     */
    public function яПерехожуПоСсылкеДляАвторизации()
    {
        $this->visit('/login');
    }

    /**
     * @When /^я авторизуюсь$/
     * @param TableNode $table
     */
    public function яАвторизуюсь(TableNode $table)
    {
        $this->fillFields($table);
        $this->pressButton('_submit');
    }

    /**
     * @When /^я добавляю слово "([^"]*)" для перевода$/
     * @param $word
     */
    public function яДобавляюСловоДляПеревода($word)
    {
        $this->fillField('phrase', $word);
        $this->pressButton('save_button');
    }


    /**
     * @When /^я меняю локаль на русский$/
     */
    public function яМеняюЛокальНаРусский()
    {
        $this->clickLink('change-ru');
    }


    /**
     * @When /^я перехожу к фразе "([^"]*)" для перевода$/
     * @param $arg
     */
    public function яПерехожуКФразеДляПеревода($arg)
    {
        $this->clickLink($arg);
    }


    /**
     * @When /^я добавляю перевод "([^"]*)"$/
     * @param $arg
     */
    public function яДобавляюПеревод($arg)
    {
        $this->fillField('translation', $arg);
        $this->pressButton('trans_button');
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('homepage'));
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }
}
