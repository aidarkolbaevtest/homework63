<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Phrase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $phrases = $this->getDoctrine()->getRepository('AppBundle:Phrase')->findAll();

        return $this->render('@App/index.html.twig', [
            'phrases' => $phrases
        ]);
    }


    /**
     * @Route("/add", name="add_phrase")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addPhraseAction(Request $request)
    {
        if (!empty(trim($request->get('phrase')))) {
            $em = $this->getDoctrine()->getManager();
            $phrase = new Phrase();
            $phrase->translate($request->getLocale())->setPhrase($request->get('phrase'));
            $em->persist($phrase);
            $phrase->mergeNewTranslations();
            $em->flush();
        }

        return $this->redirectToRoute('homepage');
    }


    /**
     * @param int $id
     * @Route("/phrase/{id}", requirements={"id" : "\d+"}, name="details")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction($id)
    {
        $phrase = $this->getDoctrine()->getRepository('AppBundle:Phrase')->find($id);
        $languages = $phrase->getTranslations()->getKeys();
        return $this->render('@App/details.html.twig', [
            'phrase' => $phrase,
            'languages' => $languages,
            'count' => count($languages),
        ]);
    }

    /**
     * @Route("/{_locale}", requirements={"_locale" : "en|ru"}, name="change_lang")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLanguageAction(Request $request)
    {
        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * @Route("/translate/{id}", requirements={"id" : "\d+"}, name="translate")
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function translateAction(Request $request, int $id)
    {
        $lang = $request->get('lang');
        $translation = $request->get('translation');
        if (!empty(trim($lang)) && !empty(trim($translation))) {
            $phrase = $this->getDoctrine()->getRepository('AppBundle:Phrase')->find($id);
            dump($phrase->getTranslations()->getKeys());
            $phrase->translate($lang, false)->setPhrase($translation);
            $em = $this->getDoctrine()->getManager();
            $em->persist($phrase);
            $phrase->mergeNewTranslations();
            $em->flush();
        }
        return $this->redirect($request->headers->get('referer'));
    }
}
